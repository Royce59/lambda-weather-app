#! /usr/bin/env python3

# Jamse Kriete
# 10/1/2019
# Hosting 
# Dev_Op's Course

# import request module to call our AWS Lambda Weather API

import requests

# API call to a lambda function that will return the locaion.
# key word <def> function name <get_weather> ("") <empty or values see below>
# (f) in the api request allows us to add on others parameters without & & & 
# print is for humans
# return is for the pc/program

def get_location(ip):
    res = requests.get(f"https://57gzn324f5.execute-api.us-east-2.amazonaws.com/Prod/get-location?ip={ip}", verify=False).json()
    return res

def get_weather(lat, lon):
    res = requests.get(f"https://57gzn324f5.execute-api.us-east-2.amazonaws.com/Prod/get-forcast?latitude={lat}&longitude={lon}", verify=False).json()
    return res

# breaking out the dict objects into a single line
def print_forecast(weather_object):
    summary = weather_object['summary']
    temp = weather_object['temperature']
    high = weather_object['high']
    low = weather_object['low']
    humidity = weather_object['humidity']
    print(f'The weather forecast is {summary}\n')
    print(f'{temp}\n')
    print(f'Temperature: {temp}')
    print(f'High: {high}')
    print(f'Low: {low}')
    print(f"Humidity: {humidity}")


# main 
if  __name__ == "__main__":
    cords = get_location("8.8.8.8")
    lat = cords['latitude']
    lon = cords['longitude']
    weather = get_weather(lat, lon)
    print_forecast(weather)

