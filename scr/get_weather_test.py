import unittest
from get_weather import lambda_handler

class TestGetWeather(unittest.TestCase):
    def test_get_weather(self):
        mock_api_call = {
        'queryStringParameters': {
                'latitude': '42.3601', 
                'longitude': '71.0589', 
                'time': None
            }   
        }

        res = lambda_handler(mock_api_call,'')
        self.assertEqual(res['statusCode'], 200)


if __name__ == '__main__':
    unittest.main()
